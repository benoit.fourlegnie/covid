#Quelques elements de gestion de tableau de tableau pour gérer la matrice
def affiche_matrice(m):
    """
    représentation graphique de la matrice passée en paramètre
    """
    for i in range(0, len(m)):
        for j in range(0, len(m[i])):
            print(m[i][j], ' ' ,end='')
        print('\n')




def initialise_matrice(ligne, colonne, valeur):
    """
    génère et retourne une matrice, cad une liste contenant
    ligne listes contenant chacune colonne éléments tous
    identiques et égaux à valeur
    
    """
    matrice=[[valeur]*colonne for i in range(ligne)] 
    return matrice
