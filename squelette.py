#Auteurs :  Gilles COSTE
#           Benoit FOURLEGNIE
# enseignants au LGT Voltaire de Wingles
#
#TP Programmation Dynamique COVID
#


#importation des modules
from lecture import *
#Utilisation de Matrice/Lise de Liste
from matrice import *

#Distance Edition

def minimum2(a,b):
    """
    Retourne le minimum entre a et b
    """
    """
    >>> minimum2(1,2)
    1
    >>> minimum2(2,1)
    1
    """
    if a<b:
        return a
    else :
        return b
    
    #ou plus simplement
    #return min(a,b)


def minimum(a,b,c):
    """
    Retourne le minimum entre a, b et c
    """
    """
    >>> minimum(1,2,3)
    1
    >>> minimum(2,1,3)
    1
    >>> minimum(2,3,1)
    1

    """
    return minimum2(a,minimum2(b,c))
    


def valeur(E,i,j):
    """
    permet de travailler avec une matrice plus petite
    (sans la première ligne et la première colonne : 0 1 2 3 ...)
    """
    """
    >>> A=[[1,2,3],[2,3,4]]
    >>> valeur(A,1,1)
    3
    >>> valeur(A,-1,1)
    2
    """
    #print(i," ",j)
    if i<0 and j<0:
        return 0
    elif i>=0 and j>=0:
        return E[i][j]
    elif i==-1:
        return j+1
    elif j==-1:
        return i+1


def MdistanceEdition(s1,s2,matrice):
    m=len(s1)#nombre de caractères (=longueur) de la chaine de caractères s1
    n=len(s2)#nombre de caractères (=longueur) de la chaine de caractères s2

    
    for i in range (0,m):#pour i allant de 1 à m inclus
        for j in range (0,n):#pour j allant de 1 à n inclus
            alignement1=valeur(matrice,i-1,j)+1 #suppression
            alignement2=valeur(matrice,i,j-1)+1#insertion
            
            if s1[i]!=s2[j]:
                alignement3=valeur(matrice,i-1,j-1)+1 #remplacement
            elif s1[i]==s2[j]:
                alignement3=valeur(matrice,i-1,j-1)
            matrice[i][j]=minimum(alignement1,alignement2,alignement3)
    
    return matrice#on retourne la matrice modifiée
        

def distanceEdition(s1,s2,matrice):
    """
    retourne la distance d'édition entre les chaines s1 et s2 passées en paramètres
    """
    """
    >>> s4='AGORRYTNE'
    >>> s3='ALGORITHME'
    >>> m2=initialise_matrice(len(s3),len(s4),0)
    >>> distanceEdition(s3,s4,m2)
    5
    """
    #on renvoie l'élément en bas à droite de la matrice
    return MdistanceEdition(s1,s2,matrice)[len(s1)-1][len(s2)-1]
    #ou encore :
    #return MdistanceEdition(s1,s2,matrice)[-1][-1]


def inserer_tiret_dans(sequence,position):
    """
    fonction prenant en paramètre une séquence de caractères
    et un indice de position
    Cette fonction retourne la séquence de caractères passée en
    paramètre dans laquelle on a insérer un tiret "-"
    en position passée paramètre.
    """
    
    return sequence[0:position]+"-"+sequence[position::]


# Alignement utiliser la matrice d'edition
def alignement(s1,s2,matrice):
    """
    >>> s4='AGORRYTNE'
    >>> s3='ALGORITHME'
    >>> m2=initialise_matrice(len(s3),len(s4),0)
    >>> m3=MdistanceEdition(s3,s4,m2)
    >>> aligne=alignement(s3,s4,m3)
    >>> aligne[0]
    'ALGORITHME'
    >>> aligne[1]
    'A-GORRYTNE'
    >>> aligne[2]
    '|I|||RRRR|'
    """
    i=len(s1)-1
    j=len(s2)-1
    s1b=s1
    s2b=s2
    aligne=""

    while i>=0 and j>=0:
        #print(i," ",j)
        #print(aligne)
        if valeur(matrice,i,j)==valeur(matrice,i-1,j-1) and s1[i]==s2[j]:
            aligne="|"+aligne
            i=i-1
            j=j-1
        elif valeur(matrice,i,j)==valeur(matrice,i-1,j-1)+1 :
            aligne="R"+aligne
            i=i-1
            j=j-1
        
        elif valeur(matrice,i,j)==valeur(matrice,i-1,j)+1:
            s2b=inserer_tiret_dans(s2b,j+1)
            aligne="I"+aligne
            i=i-1
        elif valeur(matrice,i,j)==valeur(matrice,i,j-1)+1:
            s1b=inserer_tiret_dans(s1b,i+1)
            aligne="I"+aligne
            j=j-1
        else:
            print("cas non traité")#pour vérification ;)
            i=i-1
            j=j-1
            
    while i>=0:
        #print("cas1")
        s2b='-'+s2b
        aligne="I"+aligne
        i=i-1
    while j>=0:
        #print("cas2")
        s1b='-'+s1b
        aligne="I"+aligne
        j=j-1    

    return [s1b,s2b,aligne]


    
def covid(nom_fichier_humain,nom_fichier_animal):
    animal=lecture(nom_fichier_animal)
    humain=lecture(nom_fichier_humain)
    a=len(animal) # nombre de lignes du fichier
    h=len(humain) # nombre de lignes du fichier
    dtot = 0
    for i in range(0,min(a,h)):
        s=humain[i]
        t=animal[i]
        m=initialise_matrice(len(s),len(t),0)
        d=distanceEdition(s,t,m)
        dtot += d
        #print("La distance d'edition entre",s, "et ", t, "est de ",d)
        #mm=MdistanceEdition(s,t,m)
        #aligne=alignement(s,t,mm)
        #print("alignement\n",aligne[0],"\n",aligne[2],"\n",aligne[1])
    #print("Distance totale : ",dtot)
    return dtot


def test_covid():
    animal1="covid/bat-SL-CoVZC45.fa"
    animal2="covid/bat-SL-CoVZXC21.fa"
    humain1="covid/2019-nCoV_WH01.fa"
    humain3="covid/2019-nCoV_WH03.fa"
    humain4="covid/2019-nCoV_WH04.fa"
    MERS="covid/MERS-CoV.fa"
    SRAS="covid/SARS-CoV.fa"
    Tab=[animal1,animal2,MERS,SRAS]
    Texte=["de la chauve-souris1","de la chauve-souris2","MERS","SRAS"]
    ########
    # moyenne des distances entre les 3 humains et ...
    ###########
    for i in range(0,4):
        d1=covid(Tab[i],humain1)
        d2=covid(Tab[i],humain3)
        d3=covid(Tab[i],humain4)
        m=(d1+d2+d3)/3
        print("moyenne des distances entre les génomes des virus des 3 humains et le génome du virus " + Texte[i] +" : ", int(m))
        #6638 est la moyenne des distances entre 2 génômes du virus des patients covidés
        #les génomes sont constitués d'environ 30000 bases azotées (ACTG)
        print(" soit une similitude des genomes de environ : " , int((30000-m+6638)/30000*100), "%")
    print("On peut donc mettre la chauce-souris1 (bat-SL-CoVZC45.fa) en garde à vue.")
    
def test_fonctions():
    """
    >>> test_fonctions()
    La distance d'edition entre ALGORITHME et  AGORRYTNE est de  5
    alignement de  ALGORITHME et  AGORRYTNE : 
     ALGORITHME 
     |I|||RRRR| 
     A-GORRYTNE
    La distance d'edition entre ALGORITHME et  AGORRYTNE est de  5
    alignement de  aagtagccactag et  aagtaagct : 
     aagtagccactag 
     |||||IIRR||II 
     aagta--agct--
    """
    s4='AGORRYTNE'
    s3='ALGORITHME'
    m2=initialise_matrice(len(s3),len(s4),0)
    
    d=distanceEdition(s3,s4,m2)
    print("La distance d'edition entre",s3, "et ", s4, "est de ",d)
    m3=MdistanceEdition(s3,s4,m2)
    #affiche_matrice(m3)
    aligne=alignement(s3,s4,m3)
    print("alignement de ",s3,"et ",s4, ": \n", aligne[0],"\n", aligne[2],"\n", aligne[1], )
    sequence1, sequence2 = "aagtagccactag", "aagtaagct"
    m4=initialise_matrice(len(sequence1),len(sequence2),0)
    d2=distanceEdition(sequence1,sequence2,m4)
    print("La distance d'edition entre",s3, "et ", s4, "est de ",d)
    m5=MdistanceEdition(sequence1,sequence2,m4)
    #affiche_matrice(m5)
    aligne=alignement(sequence1,sequence2,m5)
    print("alignement de ",sequence1,"et ",sequence2,": \n", aligne[0],"\n", aligne[2],"\n", aligne[1], )
    
#On systématise les tests
if __name__ == '__main__':
    import doctest
    doctest.testmod(verbose=True)
    
print("################")
test_fonctions()
print("################")
print("Calcul en cours...")
test_covid()
print("Fin")
print("################")
print("Gilles COSTE et Benoit FOURLEGNIE")